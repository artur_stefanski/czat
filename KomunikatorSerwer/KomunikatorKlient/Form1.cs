﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;


namespace KomunikatorKlient
{
    public partial class Form1 : Form
    {
        private TcpClient klient;
        private BinaryWriter pisanie;
        private BinaryReader czytanie;
        private string serwerIP = "127.0.0.1";
        private int pozycjaKursora=0;
        private bool polaczeniaAktywne;

        public Form1()
        {
            InitializeComponent();
            this.webBrowser1.Navigate("about:blank");
            this.webBrowser1.Document.Write("<html><head><style>body, table { fontsize: 10pt; font - family: Verdana; margin: 3px 3px 3px 3px; font - color: black; }</ style ></ head >< body width =\"" + (webBrowser1.ClientSize.Width - 20).ToString() + "\">");


        }
        private void wprowadzTag(string tag)
        {
            string kod = textBox1.Text;
            textBox1.Text = kod.Insert(pozycjaKursora, tag);
            textBox1.Focus();
            if (tag == "<br>" || tag == "<hr>")
            {
                textBox1.Select(pozycjaKursora + tag.Length, 0);
                pozycjaKursora += tag.Length;
            }
            else
            {
                textBox1.Select(pozycjaKursora + tag.Length / 2, 0);
                pozycjaKursora += tag.Length / 2;
            }
        }

        private void textBox1_MouseUp(object sender, MouseEventArgs e)
        {
            pozycjaKursora = textBox1.SelectionStart;
        }
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            pozycjaKursora = textBox1.SelectionStart;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wprowadzTag("<b></b>");

        }

        private void button4_Click(object sender, EventArgs e)
        {
            wprowadzTag("<i></i>");
        }

        private void wyczyśćToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.webBrowser1.Navigate("about:blank");
        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                    try
                    {
                        sw.Write(webBrowser1.DocumentText);
                    }
                    catch
                    {
                        MessageBox.Show("Nie można zapisać pliku: " +
                        saveFileDialog1.FileName);
                    }
            }

        }
        delegate void SetTextCallBack(string tekst);
        delegate void SetScrollCallBack();
        private void SetText(string tekst)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetText);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.listBox1.Items.Add(tekst);
            }
        }
        private void SetTextHTML(string tekst)
        {
            if (webBrowser1.InvokeRequired)
            {
                SetTextCallBack f = new SetTextCallBack(SetTextHTML);
                this.Invoke(f, new object[] { tekst });
            }
            else
            {
                this.webBrowser1.Document.Write(
                tekst);
            }
        }

        private void SetScroll()
        {
            if (webBrowser1.InvokeRequired)
            {
                SetScrollCallBack s = new SetScrollCallBack(SetScroll);
                //Action s = new Action(SetScroll);
                this.Invoke(s);
            }
            else
            {
                //!this.webBrowser1.Document.Window.ScrollTo(1, Int32.MaxValue);
                var pozycja = this.webBrowser1.Document.Body.ScrollRectangle.Height;
                this.webBrowser1.Document.Window.ScrollTo(1, pozycja);
            }
        }
        private void WpiszTekst(string kto, string wiadomosc)
        {
            SetTextHTML("<table><tr><td width=\"10%\"><b>" + kto + "</b></td><td width=\"90%\">(" + DateTime.Now.ToShortTimeString() + "):</td></tr>"); SetTextHTML("<tr><td colspan=2>" + wiadomosc + "</td></tr></table>"); SetTextHTML("<hr>");
            SetScroll();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                klient = new TcpClient(serwerIP, (int)numericUpDown1.Value);
                NetworkStream ns = klient.GetStream();
                czytanie = new BinaryReader(ns);
                pisanie = new BinaryWriter(ns);
                pisanie.Write("###HI###");
                this.SetText("Autoryzacja ...");
                polaczeniaAktywne = true;
                backgroundWorker2.RunWorkerAsync();
            }
            catch
            {
                this.SetText("Nie można nawiązać połączenia");
                polaczeniaAktywne = false;
            }

        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            SetText("Autoryzacja zakończona");
            string wiadomosc;
            try
            {
                while ((wiadomosc = czytanie.ReadString()) != "###BYE###")
                {
                    WpiszTekst("ktoś", wiadomosc);
                }
                SetText("Połączenie przerwane");
                polaczeniaAktywne = false;
                klient.Close();
            }
            catch
            {
                SetText("Połączenie z serwerem zostało przerwane");
                polaczeniaAktywne = false;
                klient.Close();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (polaczeniaAktywne == false)
            {
                backgroundWorker1.RunWorkerAsync();
                webBrowser1.Navigate("about:blank");
            }
            else
            {
                polaczeniaAktywne = false;
                if (klient != null)
                {
                    pisanie.Write("###BYE###");
                    klient.Close();
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            WpiszTekst("ja", textBox1.Text);
            if (polaczeniaAktywne)
                pisanie.Write(textBox1.Text);
            textBox1.Text = "";
            this.pozycjaKursora = 0; //!
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                this.button2_Click(sender, e);
        }

    }

}
